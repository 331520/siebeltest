/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author arbuz
 */
public class TestFF {
    
    private  static WebDriver driver = null;
    private  static SupportFactory supportFactory = new SupportFactory();
    
    public void login() throws InterruptedException{
    
        supportFactory.setDriver("gecko", "geckodriver");
        
        driver = new FirefoxDriver();
        
        driver.get("https://10.1.16.98/fins_rus_ui/start.swe");
        
        Thread.sleep(30000);
        
        WebElement weSWEUserName = driver.findElement(By.name("SWEUserName"));
        WebElement weSWEPassword = driver.findElement(By.name("SWEPassword"));
        weSWEUserName.sendKeys("aarbuzov");
        weSWEPassword.sendKeys("aarbuzov");
        driver.findElement(By.id("s_swepi_22")).sendKeys(Keys.RETURN);
        
        driver.quit();
    }
    
}
