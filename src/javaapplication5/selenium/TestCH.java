/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5.selenium;

import java.awt.AWTException;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;



/**
 *
 * @author arbuz
 */
public class TestCH {
    
    
    private  static WebDriver driver = null;
    private  static SupportFactory supportFactory = new SupportFactory();
    
    public void login() throws InterruptedException, AWTException, IOException{
     
        driver = supportFactory.login("aarbuzov", "aarbuzov");
        
        //Физ Лица
        driver.get("https://10.1.16.98/fins_rus_ui/start.swe?SWECmd=GotoView&SWEView=EGR+Contact+Screen+Homepage+View");
        //Физ Лица - созданные мной
        driver.get("https://10.1.16.98/fins_rus_ui/start.swe?SWECmd=GotoView&SWEView=EGR+Contact+Main+View+-+My");
        Thread.sleep(5000);

        
        //Создание
        driver.findElement(By.id("s_1_1_70_0_Ctrl")).click();
        Thread.sleep(1000);
        supportFactory.takeScreenSL(driver);
        
        //Фамилия
        driver.findElement(By.name("s_1_1_17_0")).sendKeys("ЗЛОВУНОВ");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
               
        //Имя
        driver.findElement(By.name("s_1_1_19_0")).sendKeys("АРЧИБАЛЬД");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //Отчество
        driver.findElement(By.name("s_1_1_21_0")).sendKeys("КОНЕВИЧ");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //Пол
        driver.findElement(By.name("s_1_1_34_0")).sendKeys("Жіноча");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //Рез-ность
        driver.findElement(By.name("s_1_1_25_0")).sendKeys("Нерезидент");
        driver.findElement(By.name("s_1_1_25_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //Инн
        driver.findElement(By.name("s_1_1_27_0")).sendKeys("0000000000");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //Дата рождения
        driver.findElement(By.name("s_1_1_30_0")).sendKeys("10.12.1986");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //Гражданство
        //driver.findElement(By.name("s_1_1_23_0")).sendKeys("Україна");
        //driver.findElement(By.name("s_1_1_23_0")).sendKeys(Keys.TAB);
        
        //место рождения
        driver.findElement(By.name("s_1_1_32_0")).sendKeys("ВАШИНГТОН");
        Thread.sleep(1000);
        supportFactory.takeScreenSL(driver);
        
        /*
        //МВГ Адреса
        driver.findElement(By.id("s_1_1_46_0_icon")).click();
        Thread.sleep(1000);
        supportFactory.takeScreenSL(driver);
        
        //Создание адреса
        driver.findElement(By.id("s_3_1_70_0_Ctrl")).click();
        Thread.sleep(1000);
        supportFactory.takeScreenSL(driver);
        
        //Тип регистрации
        driver.findElement(By.name("s_3_2_89_0")).sendKeys(Keys.chord(Keys.SHIFT, Keys.HOME));
        Thread.sleep(100);
        driver.findElement(By.name("s_3_2_89_0")).sendKeys(Keys.DELETE);
        driver.findElement(By.name("s_3_2_89_0")).sendKeys("Перебування (тимчасового переб");
        driver.findElement(By.name("s_3_2_89_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //Индекс
        driver.findElement(By.name("s_3_2_91_0")).sendKeys("12345");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);

        //Область
        driver.findElement(By.name("s_3_2_92_0")).sendKeys("Вiнницька область");
        driver.findElement(By.name("s_3_2_92_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);

        //Район
        driver.findElement(By.name("s_3_2_93_0")).sendKeys("Вiнницький");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);

        //тип НП
        driver.findElement(By.name("s_3_2_94_0")).sendKeys("мiсто");
        driver.findElement(By.name("s_3_2_94_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //НП
        driver.findElement(By.name("s_3_2_95_0")).sendKeys("Вiнниця");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //тип улицы
        driver.findElement(By.name("s_3_2_96_0")).sendKeys("бульвар");
        driver.findElement(By.name("s_3_2_96_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //улица
        driver.findElement(By.name("s_3_2_97_0")).sendKeys("Вiнницькаяяяяя");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //тип дома
        driver.findElement(By.name("s_3_2_98_0")).sendKeys("будинок");
        driver.findElement(By.name("s_3_2_98_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //дом
        driver.findElement(By.name("s_3_2_99_0")).sendKeys("100");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //корпус
        driver.findElement(By.name("s_3_2_100_0")).sendKeys("1");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //тип помещения
        driver.findElement(By.name("s_3_2_101_0")).sendKeys("кімната");
        driver.findElement(By.name("s_3_2_101_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //номер помещения
        driver.findElement(By.name("s_3_2_102_0")).sendKeys("1");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //срок проживания
        driver.findElement(By.name("s_3_2_103_0")).sendKeys("200");
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        //Совпадает
        driver.findElement(By.name("s_3_2_104_0")).click();
        Thread.sleep(1000);
        supportFactory.takeScreenSL(driver);
        
        //Сохранить
        driver.findElement(By.id("s_3_1_75_0_Ctrl")).click();
        Thread.sleep(1000);
        supportFactory.takeScreen();
        
        
        //Закрыть
        driver.findElement(By.id("s_3_1_81_0_Ctrl")).click();
        //supportFactory.takeScreenSL(driver);
        supportFactory.takeScreen();
        Thread.sleep(1000);
        //supportFactory.takeScreenSL(driver);
        supportFactory.takeScreen();
        */
        
        //Документ - иконка
        driver.findElement(By.id("s_1_1_8_0_icon")).click();
        Thread.sleep(2000);
        supportFactory.takeScreen();
        
        //Документ - создать
        driver.findElement(By.name("s_3_1_81_0")).click();
        Thread.sleep(100);
        supportFactory.takeScreenSL(driver);
        
        
        //WebElement newCnt = driver.findElement(By.name("s_2_1_8_0"));
        //System.out.println("newCnt : " + newCnt);
        //newCnt.click();
        //s_1_1_70_0_Ctrl
        
        // Alternatively the same thing can be done like this
        //driver.navigate().to("http://www.google.com");
        //driver.quit();
    
    }
    
    
}
