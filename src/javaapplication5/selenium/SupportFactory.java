/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication5.selenium;


import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author amigo
 */
public class SupportFactory {

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
    
    public void setDriver(String browser, String driver) {
        if (System.getProperty("os.name").contains("Windows")) {
            System.setProperty("webdriver." + browser + ".driver", "S:\\javaPRG\\siebeltest\\libs\\" + driver + ".exe");
        } else {
            System.setProperty("webdriver." + browser + ".driver", "/home/arbuz/NetBeansProjects/JavaApplication5-selenium/libs/" + driver + "");
        }
    }



    //Логин в сибель
    public WebDriver login(String l, String p) {

        WebDriver driver = null;
        try {

            // Create a new instance of the Firefox driver
            // Notice that the remainder of the code relies on the interface, 
            // not the implementation.
            setDriver("chrome", "chromedriver");

            driver = new ChromeDriver();
            // And now use this to visit Google
            //driver.get("https://www.google.com.ua");
            driver.get("https://10.1.16.98/fins_rus_ui/start.swe");

            WebElement weSWEUserName = driver.findElement(By.name("SWEUserName"));
            WebElement weSWEPassword = driver.findElement(By.name("SWEPassword"));
            weSWEUserName.sendKeys(l);
            weSWEPassword.sendKeys(p);
            takeScreenSL(driver);
            driver.findElement(By.id("s_swepi_22")).sendKeys(Keys.RETURN);
            

        } catch (Exception e) {

            System.err.println("Ошибка входа в Siebel: " + e.getMessage());
            driver.quit();

        }

        return driver;
    }
    
    //фоточка
    public void takeScreen() throws AWTException, IOException{
    
    Date date = new Date();
    //System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43 
    BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
        ImageIO.write(image, "png", new File("log/"+dateFormat.format(date)+".png"));
    }

    //фоточка от селениума
    public void takeScreenSL(WebDriver driver) throws AWTException, IOException{
    Date date = new Date();
    //System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43 
    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
// Now you can do whatever you need to do with it, for example copy somewhere
    FileUtils.copyFile(scrFile, new File("log/"+dateFormat.format(date)+".png"));
    }

}
